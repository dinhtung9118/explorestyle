import Base from './base';
import Layout from './layout';
const Styled = {
  FontsHelveticaNeue: Base.Fonts.HelveticaNeue,
  Color: Base.Color,
  Layout: Layout,
  Margin: Base.Margin,
};

export default Styled;
