import {Dimensions, Platform, StatusBar} from 'react-native';
const bottomBarIphoneX = 34;
const statusBarIphoneX = 44;
const navigationBarHeightIphoneX = 88;
const {width, height} = Dimensions.get('window');
const screenWidth = width < height ? width : height;
const screenHeight = width < height ? height : width;
const iPhoneX = screenHeight === 812;
const statusbarHeight = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const navBarHeight = Platform.OS === 'ios' ? 64 : 54;
const tabBarHeight = 50;
const Layout = {
  screenWidth,
  screenHeight,
  statusbarHeight: iPhoneX ? statusBarIphoneX : statusbarHeight,
  navBarHeight: iPhoneX ? navigationBarHeightIphoneX : navBarHeight,
  buttonRadius: 4,
  cardRadius: 5,
  tabBarHeight: iPhoneX ? bottomBarIphoneX + tabBarHeight : tabBarHeight,
  bottomBar: iPhoneX ? bottomBarIphoneX : 0,
  boxShadowButton: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
};

export default Layout;
