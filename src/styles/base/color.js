const Colors = {
  black: '#000',
  white: '#FFFFFF',
  primary: '#50b4b6',
  red: '#FF0000',
  grey: '#cccccc',
  gray: '#eeedf3',
  gray80: '#cccccc',
  brown: '#42424f',
  transparent: 'transparent',
};

export default Colors;
