const Margins = {
  /**
   * 8
   */
  x1: 8,

  /**
   * 16
   */
  x2: 16,

  /**
   * 24
   */
  x3: 24,

  /**
   * 32
   */
  x4: 32,

  /**
   * 48
   */
  x5: 48,
};
export default Margins;
