import Fonts from './font';
import Margins from './margin';
import Color from './color';
const Base = {
  Fonts: Fonts,
  Margin: Margins,
  Color: Color,
};
export default Base;
