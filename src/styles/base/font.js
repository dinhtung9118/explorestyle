const Fonts = {
  base: 'HelveticaNeue',
  HelveticaNeue: {
    Thin: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '200',
    },
    Light: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '300',
    },
    Roman: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '400',
    },
    Medium: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '500',
    },
    Bold: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '600',
    },
    Heavy: {
      fontFamily: 'HelveticaNeue',
      fontWeight: '700',
    },
  },
};

export default Fonts;
