import * as React from "react";
import { ActivityIndicator, TextInputProps } from "react-native";
import styled from "styled-components/native";
import { Fontello, IconName } from "../components/Fontello.component";
import { theme } from "../common/common";
import { getPhoneNumber } from "../utils/phone.utils";
import { removeEmptySpaces } from "../utils/string.utils";

type IProps = {
    errorMessage?: string;
    showUnderlineErrorColor?: boolean;
    editable?: boolean;
    containerStyle: any;
    placeholder?: string;
    floatingPlaceholder?: string;
    showClear?: boolean;
    loading?: boolean;
    required?: boolean;
    backgroundColor: string /* used for floatingPlaceholder */;
    suffixIcon?: IconName;
    suffixSize: number;
    suffixColor?: string;
    verticalPadding: number;
    horizontalPadding: number;
    onChangeText: (value: string, error?: boolean) => void;
} & TextInputProps;

export class Input extends React.Component<IProps> {
    textInput = null as any | null;
    static defaultProps = {
        containerStyle: {},
        backgroundColor: theme.colors.lightGrey,
        horizontalPadding: 10,
        verticalPadding: 10,
        suffixSize: 20
    };
    state = {
        hasFocus: false,
        secureTextEntry: this.props.textContentType === "password"
    };
    toggleSecureTextEntry = () => {
        this.setState({ secureTextEntry: !this.state.secureTextEntry });
    };
    onFocus = () => {
        this.setState({ hasFocus: true });
    };
    onBlur = () => {
        this.setState({ hasFocus: false });
    };
    onChangeText = (value: string) => {
        let phoneNumber;
        if (this.props.keyboardType === "phone-pad") {
            const cleanValue = removeEmptySpaces(value);
            let limit = 10;
            if (cleanValue.includes("+33")) {
                limit = 12;
            }
            if (cleanValue.length > limit) {
                return;
            }
            phoneNumber = getPhoneNumber(value);
            if (phoneNumber && phoneNumber.number) {
                value = phoneNumber.number;
            }
        }
        this.props.onChangeText && this.props.onChangeText(value, !phoneNumber);
    };
    clear = () => {
        this.props.onChangeText && this.props.onChangeText("");
    };
    showPasswordIcon = () => {
        return (
            this.state.secureTextEntry && this.props.textContentType === "password"
        );
    };
    showPasswordIconHide = () => {
        return (
            !this.state.secureTextEntry && this.props.textContentType === "password"
        );
    };
    showLoading = () => {
        return this.props.loading && this.props.textContentType !== "password";
    };
    showClear = () => {
        return (
            this.props.showClear &&
            this.props.value &&
            this.props.value.length > 0 &&
            !this.props.loading &&
            this.props.textContentType !== "password"
        );
    };

    isEmpty = () => {
        return !this.props.value || !this.props.value.length;
    };

    getInputColor = (): string => {
        const { errorMessage, showUnderlineErrorColor } = this.props;
        const inputColor =
            errorMessage || showUnderlineErrorColor
                ? theme.colors.scarlet
                : this.props.editable !== false &&
                (this.state.hasFocus || (this.props.value && this.props.value.length))
                ? theme.colors.sea
                : theme.colors.brownGrey;
        return inputColor;
    };

    renderSuffix = () => {
        const { suffixColor } = this.props;
        const inputColor = this.getInputColor();
        return (
            <IconContainer>
                {!!this.props.suffixIcon && (
                    <Fontello
                        size={this.props.suffixSize}
                        name={this.props.suffixIcon}
                        color={suffixColor || inputColor}
                        onPress={this.toggleSecureTextEntry}
                    />
                )}
                {this.showPasswordIcon() && (
                    <Fontello
                        size={20}
                        name="show"
                        color={theme.colors.brownGrey}
                        onPress={this.toggleSecureTextEntry}
                    />
                )}
                {this.showPasswordIconHide() && (
                    <Fontello
                        size={20}
                        name="hide"
                        color={theme.colors.brownGrey}
                        onPress={this.toggleSecureTextEntry}
                    />
                )}
                {!!this.showClear() && (
                    <Fontello
                        size={15}
                        name="cross"
                        color={theme.colors.brownGrey}
                        onPress={this.clear}
                    />
                )}
                {this.showLoading() && (
                    <ActivityIndicator
                        style={{ position: "relative", bottom: 4 }}
                        size="small"
                        color={theme.colors.brownGrey}
                    />
                )}
            </IconContainer>
        );
    };

    public render() {
        const {
            errorMessage,
            showUnderlineErrorColor,
            placeholder,
            containerStyle,
            backgroundColor,
            ...rest
        } = this.props;
        const floatingPlaceholder =
            this.props.floatingPlaceholder || this.props.placeholder;
        const inputColor = this.getInputColor();

        return (
            <RowContainer borderColor={inputColor} style={containerStyle}>
                {!!(
                    (this.props.required || this.state.hasFocus || !this.isEmpty()) &&
                    floatingPlaceholder
                ) && (
                    <FloatingPlaceholder backgroundColor={backgroundColor}>
                        <FloatingPlaceholderText color={inputColor}>
                            {floatingPlaceholder}
                        </FloatingPlaceholderText>
                        {this.props.required && <RequiredStar>*</RequiredStar>}
                    </FloatingPlaceholder>
                )}
                <TextInput
                    ref={ref => (this.textInput = ref)}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    placeholderTextColor={theme.colors.brownGrey}
                    placeholder={placeholder}
                    textContentType={this.props.textContentType}
                    horizontalPadding={this.props.horizontalPadding}
                    verticalPadding={this.props.verticalPadding}
                    {...rest}
                    onChangeText={this.onChangeText}
                    secureTextEntry={this.state.secureTextEntry}
                />
                <ErrorMessage>{this.props.errorMessage}</ErrorMessage>
                {this.renderSuffix()}
            </RowContainer>
        );
    }
}


