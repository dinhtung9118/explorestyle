const RequiredStar = styled.Text`
                     color: ${theme.colors.scarlet};
font-size: 11px;
font-family: ${theme.fonts.regular};
margin-left: 2px;
`;

const IconContainer = styled.View`
                      position: absolute;
right: 10px;
`;

interface IRowProps {
    borderColor: string;
}

const RowContainer = styled.View`
                     flex-direction: row;
justify-content: center;
align-items: center;
border: 1px solid ${(props: IRowProps) => props.borderColor}};
border-radius: 3px;
`;

interface ITextInputProps {
    textContentType: TextInputProps["textContentType"];
    editable?: boolean;
    verticalPadding: number;
    horizontalPadding: number;
}

const TextInput = styled.TextInput`
                  text-align: left;
width: 100%;
height: 40px;
font-size: 13px;
color: ${(props: ITextInputProps) =>
    props.editable === false
        ? theme.colors.brownishGrey
        : theme.colors.secondary};
padding: ${(props: ITextInputProps) =>
    `${props.verticalPadding}px ${props.horizontalPadding}px`};
opacity: ${(props: ITextInputProps) => (props.editable === false ? 0.7 : 1)};
`;

interface IFloatingPlaceholderProps {
    backgroundColor: string;
}

const FloatingPlaceholder = styled.View`
                            background-color: ${(props: IFloatingPlaceholderProps) =>
    props.backgroundColor};
position: absolute;
top: -8px;
left: 8px;
padding: 0px 3px;
z-index: 10;
flex-direction: row;
justify-content: center;
align-items: center;
`;

interface IFloatingPlaceholderTextProps {
    color: string;
}

const FloatingPlaceholderText = styled.Text`
                                font-size: 11px;
font-family: ${theme.fonts.regular};
color: ${(props: IFloatingPlaceholderTextProps) => props.color};
`;

const ErrorMessage = styled.Text`
                     position: absolute;
bottom: -18px;
left: 10px;
font-family: ${theme.fonts.regular};
font-size: 11;
text-align: left;
letter-spacing: 0;
color: ${theme.colors.scarlet};
`;
