import {Text, TouchableOpacity} from 'react-native';
import glamorous from 'glamorous-native';
import Styled from '../../styles';
const stylesButton = props => ({
  opacity: props.disabled ? 0.5 : 1,
  backgroundColor: props.color,
  paddingTop: props.verticalPadding,
  paddingBottom: props.verticalPadding,
  paddingRight: props.horizontalPadding,
  paddingLeft: props.horizontalPadding,
  borderWidth: props.border ? 1 : 0,
  borderColor: props.border ? props.borderColor : 'transparent',
  height: props.height,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: props.icon ? 'space-between' : 'center',
  width: props.width ? props.width : 'auto',
  borderRadius: Styled.Layout.buttonRadius,
});
export const StyledButton = glamorous(TouchableOpacity)(stylesButton);

export const stylesText = props => ({
  color: props.color,
  alignItems: 'center',
  alignContent: 'center',
  fontSize: props.fontSize,
});

export const StyledText = glamorous(Text)(stylesText);
