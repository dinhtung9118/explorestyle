import styled from 'styled-components/native';
import Styled from '../../styles';

export const StyledButton = styled.TouchableOpacity`
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  ${props =>
    props.border &&
    `border: 1px ${props.borderColor || props.textColor} ${props.borderStyle};`}
  border-radius: ${Styled.Layout.buttonRadius};
  background-color: ${props => props.color};
  padding-right: ${props => props.horizontalPadding};
  padding-left: ${props => props.horizontalPadding};
  padding-top: ${props => props.verticalPadding};
  padding-bottom: ${props => props.verticalPadding};
  flex-direction: row;
  height: ${props => props.height};
  align-items: center;
  justify-content: ${props => (props.icon ? 'space-between' : 'center')};
  ${props => props.width && `width: ${props.width}`};
  box-shadow: 1px 1px #888888;
`;

export const StyledText = styled.Text`
  color: ${props => props.color};
  text-align: center;
  align-self: center;
  font-size: ${props => props.fontSize}px;
`;
