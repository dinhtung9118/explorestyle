import React from 'react';
import renderer from 'react-test-renderer';
import AddButton from './Button.component.glamorous';
import {StyledButton, StyledText} from './Button.components.glamorous.style';
it('AppButton renders correctly', () => {
  const tree = renderer.create(<AddButton text={'submit'} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('StyledButton renders correctly', () => {
  const tree = renderer.create(<StyledButton text={'submit'} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('StyleText renders correctly', () => {
  const tree = renderer.create(<StyledText text={'submit'} />).toJSON();
  expect(tree).toMatchSnapshot();
});
