import React, {Fragment} from 'react';
import {Text, View, ActivityIndicator, Image} from 'react-native';
import PropTypes from 'prop-types';

import Styled from '../../styles';

import {
  StyledButton,
  StyledText,
  StyledWrapperTextInButton,
} from './Button.component.styles';

const propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func,
  fontFamily: PropTypes.string,
  fontSize: PropTypes.number,
  leftIcon: PropTypes.string,
  rightIcon: PropTypes.string,
  disabled: PropTypes.bool,
  horizontalPadding: PropTypes.number,
  verticalPadding: PropTypes.number,
  height: PropTypes.number,
  width: PropTypes.number,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  border: PropTypes.bool,
  isLoading: PropTypes.bool,
  activeOpacity: PropTypes.number,
  iconSize: PropTypes.number,
  borderStyle: PropTypes.string,
  borderColor: PropTypes.string,
  marginBetweenTextAndIcon: PropTypes.number,
  raised: PropTypes.bool,
  style: PropTypes.object,
};

const propTypesText = {
  color: PropTypes.string,
  fontSize: PropTypes.number,
};

const defaultProps = {
  disabled: false,
  height: 35,
  fontSize: 15,
  horizontalPadding: 16,
  verticalPadding: 6,
  textColor: Styled.Color.white,
  backgroundColor: Styled.Color.primary,
  border: false,
  borderStyle: 'solid',
  isLoading: false,
  activeOpacity: 0.2,
  marginBetweenTextAndIcon: 0,
  width: 120,
  raised: false,
  style: {},
};
function TextInButton(props) {
  const {
    isLoading,
    textColor,
    leftIcon,
    rightIcon,
    fontFamily,
    text,
    iconSize,
    fontSize,
  } = props;
  if (isLoading) {
    return <ActivityIndicator size="small" color={textColor} />;
  }
  if (leftIcon || rightIcon) {
    return (
      <Fragment>
        {leftIcon && (
          <Image
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/84/84380.png',
            }}
            style={{
              width: 16,
              height: 16,
              marginRight: 5,
            }}
          />
        )}
        <StyledText
          fontFamily={fontFamily}
          color={textColor}
          fontSize={fontSize}>
          {text}
        </StyledText>
        {rightIcon && (
          <Image
            source={{
              uri: 'https://image.flaticon.com/icons/png/512/84/84380.png',
            }}
            width={30}
            height={30}
          />
        )}
      </Fragment>
    );
  }
  return (
    <StyledText
      numberOfLines={2}
      fontFamily={fontFamily}
      color={textColor}
      fontSize={fontSize}>
      {text}
    </StyledText>
  );
}

export default function AppButton(props) {
  const {
    leftIcon,
    rightIcon,
    onPress,
    disabled,
    height,
    width,
    horizontalPadding,
    verticalPadding,
    backgroundColor,
    border,
    isLoading,
    textColor,
    fontFamily,
    fontSize,
    text,
    iconSize,
    style,
    borderStyle,
    borderColor,
    raised,
  } = props;
  return (
    <Fragment>
      <StyledButton
        onPress={onPress}
        icon={leftIcon || rightIcon}
        disabled={disabled}
        height={height}
        width={width}
        horizontalPadding={horizontalPadding}
        verticalPadding={verticalPadding}
        border={border}
        color={backgroundColor}
        textColor={textColor}
        activeOpacity={props.activeOpacity}
        borderStyle={borderStyle}
        borderColor={borderColor}
        raised={raised}
        style={style}>
        <TextInButton
          isLoading={isLoading}
          textColor={textColor}
          fontFamily={fontFamily}
          text={text}
          leftIcon={leftIcon}
          rightIcon={rightIcon}
          iconSize={iconSize}
          fontSize={fontSize}
        />
      </StyledButton>
    </Fragment>
  );
}
AppButton.propsTypes = propTypes;
AppButton.defaultProps = defaultProps;
TextInButton.propsTypes = propTypesText;
