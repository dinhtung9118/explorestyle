import React from "react";
import { Animated } from "react-native";
import Collapsible from "react-native-collapsible";
import styled from "styled-components/native";
import glamorous, {withTheme} from 'glamorous-native'
import { theme } from "../theme/theme";
import { Chips, ChipsContainer } from "./Chips.component";
import { Fontello, IconName } from "./Fontello.component";
import { RequiredStar } from "./RequiredStar.component";

// interface IProps {
//     name: string;
//     icon?: IconName;
//     iconCollapsed?: IconName;
//     title: string;
//     isRequired?: boolean;
//     isCollapsed?: boolean;
//     collapsibledTitleLeft?: string;
//     collapsibledTitleRight?: (string | undefined) | Array<string | undefined>;
//     onPress: (name: string) => void;
//     onOpenedPress?: (name: string) => void;
// }

export class Card extends React.Component<IProps, { height: Animated.Value }> {
    static defaultProps = {
        onPress: (name: string) => undefined
    };

    onCardPress = (name: string) => () => {
        if (!this.props.isCollapsed) {
            this.props.onOpenedPress && this.props.onOpenedPress(name);
            return;
        }
        this.props.onPress(name);
    };
    public render() {
        const {
            title,
            icon,
            iconCollapsed,
            isRequired,
            isCollapsed,
            collapsibledTitleLeft,
            name
        } = this.props;
        let collapsibledTitleRight: string[] = [];
        if (
            this.props.collapsibledTitleRight &&
            !Array.isArray(this.props.collapsibledTitleRight)
        ) {
            collapsibledTitleRight = new Array(this.props.collapsibledTitleRight);
        } else if (this.props.collapsibledTitleRight) {
            collapsibledTitleRight = this.props.collapsibledTitleRight;
        }
        return (
            <CardContainer onPress={this.onCardPress(name)} activeOpacity={1}>
                <React.Fragment>
                    {isCollapsed ? (
                        <TitleContainer>
                            <Row>
                                {iconCollapsed && (
                                    <IconContainer>
                                        <Fontello
                                            name={iconCollapsed}
                                            size={15}
                                            color={theme.colors.secondary}
                                            style={{ marginRight: 10 }}
                                        />
                                    </IconContainer>
                                )}
                                <CollaspibleTitleLeft>
                                    {collapsibledTitleLeft} {isRequired && <RequiredStar />}
                                </CollaspibleTitleLeft>
                            </Row>
                            <CollaspibleTitleRight>
                                <ChipsContainer justifyContent="flex-end">
                                    {collapsibledTitleRight &&
                                    collapsibledTitleRight.map(
                                        (text: string) =>
                                            text && <Chips key={text} id={text} text={text} />
                                    )}
                                </ChipsContainer>
                            </CollaspibleTitleRight>
                        </TitleContainer>
                    ) : (
                        <TitleContainer>
                            {icon && (
                                <IconContainer>
                                    <Fontello
                                        name={icon}
                                        size={15}
                                        color={theme.colors.secondary}
                                        style={{ marginRight: 10 }}
                                    />
                                </IconContainer>
                            )}
                            <Title numberOfLines={2}>
                                {title} {isRequired && <RequiredStar />}
                            </Title>
                        </TitleContainer>
                    )}

                    {this.props.children && (
                        <Collapsible collapsed={isCollapsed} duration={500}>
                            <ChildrenContainer>{this.props.children}</ChildrenContainer>
                        </Collapsible>
                    )}
                </React.Fragment>
            </CardContainer>
        );
    }
}

const TitleContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-bottom: ${theme.margins.x2};
  overflow: hidden;
`;

const IconContainer = styled.View`
  padding-top: 2.5px;
  padding-bottom: 2.5px;
  top: -1px;
`;

const CollaspibleTitleLeft = styled.Text`
  font-family: ${theme.fonts.regular};
  font-size: 14;
  line-height: 20px;
  min-height: 20px;
  color: ${theme.colors.secondary};
  padding-right: 10px;
`;

const Row = styled.View`
  flex-direction: row;
`;

const CollaspibleTitleRight = styled.View`
  flex-grow: 1;
  flex-shrink: 1;
`;

// const CollaspibleTitleRight = styled.Text`
//   background-color: red;
//   font-family: ${theme.fonts.regular};
//   font-size: 13;
//   line-height: 20px;
//   min-height: 20px;
//   color: ${theme.colors.sea};
//   margin-left: ${theme.margins.x1};
//   flex-shrink: 1;
// `;

const Title = styled.Text`
  font-family: ${theme.fonts.bold};
  font-size: 15;
  line-height: 20px;
  min-height: 20px;
  color: ${theme.colors.secondary};
  flex-wrap: wrap;
  flex: 1;
`;

const CardContainer = styled.TouchableOpacity`
  padding: 16px;
  padding-bottom: 0;
  width: 264;
  border-radius: 3;
  background-color: #fff;
  elevation: 2;
  shadow-color: rgba(0, 0, 0, 0.12);
  shadow-radius: 8;
  shadow-opacity: 1;
  width: 100%;
`;

const ChildrenContainer = styled.View`
  padding-bottom: ${theme.margins.x2};
`;
