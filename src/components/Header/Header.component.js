import React from 'react';
import {View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Styled from '../../styles';
import {
  StyledText,
  InvisibleContainerLeft,
  InvisibleContainerRight,
} from './Header.component.styles';

const propStyleHeader = {
  leftSize: PropTypes.number,
  renderLeft: PropTypes.element,
  rightSize: PropTypes.number,
  renderRight: PropTypes.element,
  title: PropTypes.string,
  background: PropTypes.string,
  paddingTop: PropTypes.number,
  paddingBottom: PropTypes.number,
};
const defaultPropsHeader = {
  paddingTop: Styled.Layout.statusbarHeight,
  paddingBottom: 12,
};
function Header(props) {
  const {
    title,
    background,
    renderLeft,
    leftSize,
    renderRight,
    rightSize,
    paddingBottom,
    paddingTop,
  } = props;

  const hasTitle = !!title;
  return (
    <View
      style={{
        paddingTop,
        paddingBottom,
        backgroundColor: background ? background : 'transparent',
        ...styles.containerHeader,
      }}>
      <InvisibleContainerLeft isSmall={hasTitle} size={leftSize}>
        {renderLeft}
      </InvisibleContainerLeft>
      {hasTitle && <StyledText>{title}</StyledText>}
      <InvisibleContainerRight isSmall={hasTitle} size={rightSize}>
        {renderRight}
      </InvisibleContainerRight>
    </View>
  );
}

Header.propsTypes = propStyleHeader;
Header.defaultProps = defaultPropsHeader;
export default Header;

const styles = StyleSheet.create({
  containerHeader: {
    paddingLeft: Styled.Margin.x3,
    paddingRight: Styled.Margin.x3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    zIndex: 3,
  },
});
