import styled, {css} from 'styled-components/native';
import Styled from '../../styles';
export const StyledText = styled.Text`
  font-weight: ${Styled.FontsHelveticaNeue.Bold.fontWeight};
  font-size: 15;
  text-align: center;
  color: ${Styled.Color.white};
  text-shadow-radius: 0;
  text-shadow-offset: 0;
`;

export const InvisibleContainerLeft = styled.View`
  ${(props: {isSmall: boolean, size?: number}) =>
    props.isSmall
      ? css`
          width: ${props.size ? props.size : '50px'};
        `
      : css`
          max-width: ${props.size ? props.size : '200px'};
        `}
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const InvisibleContainerRight = styled.View`
  ${(props: {isSmall: boolean, size?: number}) =>
    props.isSmall
      ? css`
          width: ${props.size ? props.size : '50px'};
        `
      : css`
          max-width: ${props.size ? props.size : '200px'};
        `}
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
