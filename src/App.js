/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {StyleSheet, StatusBar, View, Text} from 'react-native';
import Styled from './styles';
import AppButton from './components/Button/Button.component';
import AppButtonGlamorous from './components/Button/Button.component.glamorous';
import Header from './components/Header/Header.component';

const App: () => React$Node = () => {
  return (
    <Fragment>
      <Header background={Styled.Color.primary} title={'Home'} />
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <AppButton
          style={styles.marginButtom}
          text={'Submit'}
          // textColor={Styled.Base.Color.red}
          // backgroundColor={Styled.Base.Color.brown}
        />
        <AppButton
          style={styles.marginButtom}
          text={'Submit'}
          disabled
          // textColor={Styled.Base.Color.red}
          // backgroundColor={Styled.Base.Color.brown}
        />
        <AppButton
          style={styles.marginButtom}
          text={'Submit'}
          leftIcon={'edit'}
          // textColor={Styled.Base.Color.red}
          // backgroundColor={Styled.Base.Color.brown}
        />
        <AppButton style={styles.marginButtom} isLoading text={'Submit'} />
        <Text>Glamorous</Text>
        <AppButtonGlamorous text={'Submit'} style={styles.marginButtom} />
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  marginButtom: {
    margin: Styled.Margin.x1,
  },
});

export default App;
